--Thai Thien
--1351040
-----------Create Company�s Project Management database



CREATE DATABASE HW01_ProjectManagement_1351040 
go


USE   HW01_ProjectManagement_1351040 
go

--Table: DEPARTMENT
--DName: nvarchar(50)
--DNumber: int
--MgrSSN: char(9)
--MgrStartDate: date
CREATE TABLE DEPARTMENT(
	DName nvarchar(50),
	DNumber int PRIMARY KEY,
	MgrSSN char(9) ,
	MgrStartDate date CHECK (MgrStartDate <= GETDATE()),
)
go

--Table: EMPLOYEE
--FName: nvarchar(50)
--Minit: char(1)
--LName: nvarchar(50)
--SSN: char(9)
--BDate: date
--Address: nvarchar(225)
--Sex: char(1)
--Salary: int
--SupperSSN: char(9)
--DNO: int
CREATE TABLE EMPLOYEE(
	FName nvarchar(50),
	Minit char(1),
	LName nvarchar(50),
	SSN char(9) PRIMARY KEY,
	BDate date CHECK (BDate <= GETDATE()),
	Address nvarchar(225),
	Sex char(1),
	Salary int ,
	SupperSSN char(9),
	DNO int,
)
go



--Table: DEPT_LOCATIONS
--DNumber: int
--DLocation: nvarchar(50)
CREATE TABLE DEPT_LOCATIONS(
	DNumber int ,
	DLocation nvarchar(50),
	PRIMARY KEY (Dnumber,DLocation),
)
go



--Table: PROJECT
--PName: nvarchar(50)
--PNumber: int
--PLocation: nvarchar(50)
--DNum: int
CREATE TABLE PROJECT(
	PName nvarchar(50),
	PNumber int PRIMARY KEY,
	PLocation nvarchar(50),
	DNum int,
)
go


--Table: WORKS_ON
--WORKS_ON: char(9)
--PNO: int
--Hours: float
CREATE TABLE WORKS_ON(
	ESSN char(9) ,
	PNO int,
	Hours float,
	PRIMARY KEY (ESSN,PNO),
)
go


--Table: DEPENDENT
--ESSN: char(9)
--Dependent_Name: nvarchar(50)
--Sex: char(1)
--BDate: date
--Relationship: nvarchar(50)
CREATE TABLE DEPENDENT(
	ESSN char(9),
	Dependent_Name nvarchar(50),
	Sex char(1) CHECK (Sex in ('M','F')),
	BDate date CHECK (BDate <= GETDATE()),
	Relationship nvarchar(50),
	PRIMARY KEY(ESSN,Dependent_Name),
)
go

--Table: PROJECT
--Foreign key 1: (DNum)
-- Reference table: DEPARTMENT
-- Reference Attributes: (DNumber)
ALTER TABLE PROJECT
ADD CONSTRAINT fk_PROJECT_DNum
FOREIGN KEY (DNum)
REFERENCES DEPARTMENT(DNumber)

--Table:  WORKS_ON
--Foreign key 2: (ESSN)
-- Reference table: EMPLOYEE
-- Reference Attributes: (SSN)
ALTER TABLE  WORKS_ON
ADD CONSTRAINT fk_WORKS_ON_ESSN
FOREIGN KEY (ESSN)
REFERENCES EMPLOYEE(SSN)

--Table:  WORKS_ON
--Foreign key 3: (PNO)
-- Reference table: PROJECT
-- Reference Attributes: (PNumber)
ALTER TABLE  WORKS_ON
ADD CONSTRAINT fk_WORKS_ON_PNO
FOREIGN KEY (PNO)
REFERENCES PROJECT(PNumber)

--Table:  DEPT_LOCATIONS
--Foreign key 3: (DNumber)
-- Reference table: DEPARTMENT
-- Reference Attributes: (DNumber)
ALTER TABLE DEPT_LOCATIONS
ADD CONSTRAINT fk_DEPT_LOCATIONS_DNumber
FOREIGN KEY (DNumber)
REFERENCES DEPARTMENT(DNumber)

--Table:  EMPLOYEE
--Foreign key 3: (DNO)
-- Reference table: DEPARTMENT
-- Reference Attributes: (DNumber)
ALTER TABLE EMPLOYEE
ADD CONSTRAINT fk_EMPLOYEE_DNO
FOREIGN KEY (DNO)
REFERENCES DEPARTMENT(DNumber)

--Table:  DEPENDENT
--Foreign key 3: (ESSN)
-- Reference table: EMPLOYEE
-- Reference Attributes: (SSN)
ALTER TABLE DEPENDENT
ADD CONSTRAINT fk_DEPENDENT_ESSN
FOREIGN KEY (ESSN)
REFERENCES EMPLOYEE(SSN)

--Table:  DEPARTMENT
--Foreign key 3: (MgrSSN)
-- Reference table: EMPLOYEE
-- Reference Attributes: (SSN)
ALTER TABLE DEPARTMENT
ADD CONSTRAINT fk_DEPARTMENT_MgrSSN
FOREIGN KEY (MgrSSN)
REFERENCES EMPLOYEE(SSN)
go

--Table:  EMPLOYEE
--Foreign key 3: (SupperSSN)
-- Reference table: EMPLOYEE
-- Reference Attributes: (SSN)
ALTER TABLE EMPLOYEE
ADD CONSTRAINT fk_EMPLOYEE_SupperSSN
FOREIGN KEY (SupperSSN)
REFERENCES EMPLOYEE(SSN)
go



-- Insert Employee table with NULL in SupperSSN, DNO
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('John','B','Smith','123456789','1965-01-09','731 Fondren, Houston, TX','M',30000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Franklin','C','Wong','333445555','1955-12-03','638 Voss, Houston, TX','M',40000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Alicia','J','Zelaya','999887777','1968-01-19','3321 Caslle, Spring, TX','F',25000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Jennifer','S','Wallace','987654321','1941-06-20','291 Berry, Bellaire, TX','F',40000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Ramesh','M','Narayan','666884444','1962-09-15','975 Fire Oak, Humble, TX','M',38000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Joyce','A','Enllen','453453453','1972-07-31','5631 Rice, Houston, TX','F',25000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Ahmad','V','Jabbar','987987987','1969-03-29','980 Dallas, Houston, TX','M',25000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('James','E','Borg','888665555','1937-11-10','50 Slone, Houston, TX','M',25000)
go

-- insert DEPARTMENT  
INSERT INTO DEPARTMENT([DName],[DNumber],[MgrSSN],[MgrStartDate])
VALUES ('Research','5','333445555','1938-05-22')
INSERT INTO DEPARTMENT([DName],[DNumber],[MgrSSN],[MgrStartDate])
VALUES ('Administration','4','987654321','1995-01-01')
INSERT INTO DEPARTMENT([DName],[DNumber],[MgrSSN],[MgrStartDate])
VALUES ('Headquarters','1','888665555','1931-06-19')
go

--Update Employee and dno in Employee table
UPDATE EMPLOYEE
SET SupperSSN='333445555',DNO=5
WHERE SSN='123456789';
UPDATE EMPLOYEE
SET SupperSSN='888665555',DNO=5
WHERE SSN='333445555';
UPDATE EMPLOYEE
SET SupperSSN='987654321',DNO=4
WHERE SSN='999887777';
UPDATE EMPLOYEE
SET SupperSSN='888665555',DNO=4
WHERE SSN='987654321';
UPDATE EMPLOYEE
SET SupperSSN='333445555',DNO=5
WHERE SSN='666884444';
UPDATE EMPLOYEE
SET SupperSSN='333445555',DNO=5
WHERE SSN='453453453';
UPDATE EMPLOYEE
SET SupperSSN='987654321',DNO=4
WHERE SSN='987987987';
UPDATE EMPLOYEE
SET DNO=1
WHERE SSN='888665555';

--Insert PROJECT 
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('Product Xbox',1,'Bellaire',5)
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('Product iPhone7',2,'Sugarland',5)
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('Product SS Mole',3,'Houslor',5)
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('Computerization',10,'Stafford',4)
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('Reorganization',20,'Houslor',1)
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('New benefits',30,'Stafford',4)
go

--Insert WORK_ON
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('123456789',1,32.5)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('123456789',2,7.5)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('666884444',3,40.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('453453453',1,20.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('453453453',2,20.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('333445555',2,10.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('333445555',3,10.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('333445555',10,10.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('333445555',20,10.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('999887777',30,30.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('999887777',10,10.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('987987987',10,35.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('987987987',30,5.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('987654321',30,20.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('987654321',20,15.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('888665555',20,null)
go

--Insert DEPENDENT
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('333445555','Alice','F','1986-04-05','Daughter')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('333445555','Theodore','M','1983-10-25','Son')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('333445555','Joy','F','1958-05-03','Spouse')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('987654321','Abner','M','1942-02-23','Spouse')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('123456789','Michael','M','1988-01-04','Son')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('123456789','Alice','F','1988-01-04','Daughter')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('123456789','Elizabeth','F','1967-05-05','Spouse')
go

--Insert DEPT_LOCATIONS
INSERT INTO DEPT_LOCATIONS([DNumber],[DLocation])
VALUES (1,'Houston')
INSERT INTO DEPT_LOCATIONS([DNumber],[DLocation])
VALUES (4,'Stafford')
INSERT INTO DEPT_LOCATIONS([DNumber],[DLocation])
VALUES (5,'Bellaire')
INSERT INTO DEPT_LOCATIONS([DNumber],[DLocation])
VALUES (5,'Sugarland')
go


--------------------------------------------------------


CREATE PROCEDURE SumTwoNumber(@varA int, @varB int)
AS	
	BEGIN
		Declare @total int;
		SET @total = @varA + @varB;
		PRINT @total;
	END



exec SumTwoNumber 13, -4
go


CREATE PROCEDURE SumBetween(@varA int, @varB int)
AS	
	BEGIN
		Declare @total int;
		SET @total =0 ;
		SET @total = @varB + @varA ;
		DECLARE @num int;
		SET @num = @varB - @varA +1;
		Declare @sum int;
		SET @sum = (@total*@num)/2;
		PRINT @sum;

	END
go

exec SumBetween 2,10
go





CREATE PROCEDURE GCD(@A int, @B int)
AS	
	BEGIN
		DECLARE @result int;
		DECLARE @tmp int;


		--swap a and b so a>=b
		if (@A <@B) 
		begin
			SET @tmp = @a;
			SET @a=@b;
			SET @B = @tmp;
		end

		--find gcd
		while (@b!= 0)
		begin
			SET @tmp = @a%@b;
			SET @a = @b;
			SET @b = @tmp;

		end
		
		SET @result = @a;
		PRINT @result;
	END

go

exec GCD 60, 80
go

CREATE PROCEDURE LCM(@A int, @B int)
AS	
	BEGIN
		DECLARE @result int;
		DECLARE @tmp1 int;
		DECLARE @tmp2 int;
		

		DECLARE @flag int;
		SET @flag = 0;
		DECLARE @i int;
		SET @i = 0;

		While (@flag = 0)
		begin
		SET @i = @i+1;
		SET @tmp1 = @i%@A;
		SET @tmp2 = @i%@B;
		if (@tmp1 = 0)
			if (@tmp2 = 0)
				SET @flag =1;
 		end
		
		SET @result = @i;
		PRINT @result;
	END

go

exec LCM 3, 4
go

CREATE PROCEDURE SumTwoNumberAndC(@varA int, @varB int, @varC int out)
AS	
	BEGIN
		Declare @total int;
		SET @total = @varA + @varB;
		SET @varC = @total;
	END
go

Declare @sum int
exec SumTwoNumberAndC 12,3,@sum output
select @sum
go

CREATE PROCEDURE SumThreeNumber(@varA int, @varB int, @varC int)
AS	
	BEGIN
		Declare @total int;
		SET @total = @varA + @varB + @varC;
		PRINT @total;
	END
go

exec SumThreeNumber 3, 4, 3 
go

CREATE FUNCTION Sum2Num(@varA int, @varB int)
Returns [int]
AS 
	BEGIN
		Declare @total int;
		SET @total = @varA + @varB;
		Return @total;
	END
go

print dbo.Sum2Num(3,4)
go

--8 Find the manager�s name of an employee base on SSN (@SSN is the input parameter).
CREATE FUNCTION FindManager(@inputSSN int)
Returns @myResult TABLE(ManagerName nvarchar(225))
AS 
	BEGIN
	Declare @name nvarchar(225);
		 SELECT  @name = e1.FName	
		 FROM EMPLOYEE as e1, EMPLOYEE as e2 
		 WHERE e2.SSN = @inputSSN and e2.SupperSSN = e1.SSN

		 Insert @myResult
		 SELECT @name

		 RETURN
	END
go

Select *
From FindManager(123456789)
go





--9 Return the number of dependents of an employee base on SSN.
CREATE FUNCTION FindNumOfDependents(@inputSSN int)
Returns @myResult TABLE(num int)
AS 
	BEGIN
	Declare @num int;
		SELECT @num = COUNT(*)
	FROM EMPLOYEE as e, DEPENDENT as d 
	Where d.ESSN = e.SSN
	and e.SSN = @inputSSN
	Group by e.FName, e.SSN

		 Insert @myResult
		 SELECT @num

		 RETURN
	END
go

Select *
From FindNumOfDependents(987654321)
go




--10. The sex of each employee should be F or M.


CREATE TRIGGER sexEmployeeTrigger
ON EMPLOYEE
FOR UPDATE, INSERT
AS 
	BEGIN
	Declare @esex nvarchar(1);
	SELECT @esex = e.Sex
	FROM inserted as e
	
	if (@esex !='M' and @esex !='F')
		rollback 

	END
go



-- 11. The employee�s salary should be between 0 and 10000. 


CREATE TRIGGER SalaryEmployeeTrigger
ON EMPLOYEE
FOR UPDATE, INSERT
AS 
	BEGIN
	Declare @salary int;
	SELECT @salary = e.Salary
	FROM inserted as e
	
	if (@salary <0 or @salary >10000)
		rollback 

	END
go

drop trigger SalaryEmployeeTrigger
go
--12. The name of departments must be unique.

CREATE TRIGGER Unique_name_department_Trigger
ON DEPARTMENT
FOR UPDATE, INSERT
AS 
	BEGIN
	
	if exists(
	SELECT  inserted.DName
	FROM inserted , DEPARTMENT as d
	WHERE inserted.DName = d.DName
	and inserted.DNumber != d.DNumber
	)
	rollback
	END
go

--Drop  trigger Unique_name_department_Trigger

-- 13. The full name of employees must be unique

CREATE TRIGGER Unique_fullname_EmployeeTrigger
ON EMPLOYEE
FOR UPDATE, INSERT
AS 
	BEGIN
	


	if   exists(
	SELECT inserted.FName
	FROM inserted, EMPLOYEE as e
	WHERE inserted.SSN != e.SSN
	and inserted.FName = e.FName
	and inserted.LName = e.LName
	and inserted.Minit = e.Minit
	)
	rollback
	END
go


--Drop  trigger Unique_fullname_EmployeeTrigger


--14. Each employee can�t manage themselves.

CREATE TRIGGER DepartmentTrigger
ON DEPARTMENT
FOR UPDATE, INSERT
AS 
	BEGIN
	
	if   exists(
	SELECT e.SSN
	FROM inserted, EMPLOYEE as e
	WHERE inserted.MgrSSN = e.SSN
	and inserted.DNumber = e.DNO
	)
	rollback
	END
go