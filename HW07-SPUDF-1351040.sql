--Thai Thien 
--1351040

USE   HW01_ProjectManagement_1351040 
go

--1 Write a stored-procedure with parameter @SSN to check if an employee�s SSN exists in database. Show the result of checking to screen with suitable message.
CREATE PROCEDURE CheckEmployee(@inputSSN int)
AS 
	BEGIN
		if exists (
		SELECT e.SSN
		FROM EMPLOYEE as e
		WHERE e.SSN = @inputSSN
		)
		print 'Exist Employee '+ convert(nvarchar,@inputSSN)
	END

	exec CheckEmployee 112
	go
	exec CheckEmployee 123456789
	go

-- 2 Write a strored-procedure to show information of an employee in following format output:
CREATE PROCEDURE ShowInformation(@inputSSN int)
AS 
	BEGIN
	DECLARE @Name nvarchar(50)
	DECLARE @DOB date
	DECLARE @Salary int
	DECLARE @DepartmentName nvarchar(50)
	DECLARE @NumOfDependent int
	DECLARE @NumOfProject int

	SELECT  @Name = CONCAT(e.FName,' ',e.Minit,' ', e.LName), @DOB = e.BDate, @Salary = e.Salary
	FROM EMPLOYEE as e
	WHERE @inputSSN = e.SSN

	SELECT @DepartmentName = d.DName
	FROM EMPLOYEE as e, DEPARTMENT as d
	WHERE e.SSN = @inputSSN
	and e.DNO = d.DNumber
	

	--num of dependent
	SELECT @NumOfDependent = COUNT(d.Dependent_Name)
	FROM EMPLOYEE as e
		LEFT JOIN DEPENDENT as d on e.SSN = d.ESSN
	WHERE e.SSN = @inputSSN
	Group by e.SSN
	


	--num of project
	SELECT @NumOfProject = COUNT(w.PNO)
	FROM EMPLOYEE as e
		LEFT JOIN WORKS_ON as w on e.SSN = w.ESSN
	WHERE  e.SSN = @inputSSN
	Group by e.SSN
	
	print 'Name: '+ @Name
	print 'Date of Birth:' + convert(nvarchar, @DOB , 120) 
	print 'Salary:' + convert(nvarchar, @Salary)
	print 'Department name: '  + @DepartmentName
	print 'Number of dependent ' + convert(nvarchar,@NumOfDependent)
	print 'Number of project ' + convert(nvarchar,@NumOfProject)
 
	END
go

exec ShowInformation 123456789
exec ShowInformation 666884444
go

-- 3 Write a stored-procedure to show all employees� information which belongs to a special department. This SP has @dname parameter which is department�s name.
CREATE PROCEDURE ShowEmployeeOfDepartment(@dname nvarchar(50))
AS
	BEGIN
	
	--Declare tmp thing
	Declare @tmpTable TABLE(SSN char(9)); -- store data already read
	DECLARE @tmpSSN char(9) -- store current ssn
	
	While (exists (SELECT Top 1 e.SSN
	FROM EMPLOYEE as e, DEPARTMENT as d
	WHERE e.DNO = d.DNumber
	and DName = @dname
	and e.SSN NOT IN (SELECT SSN FROM @tmpTable)))
		begin
	--select @tmpSSN as top employee
	SELECT Top 1 @tmpSSN = e.SSN
	FROM EMPLOYEE as e, DEPARTMENT as d
	WHERE e.DNO = d.DNumber
	and DName = @dname
	and e.SSN NOT IN (SELECT SSN FROM @tmpTable)-- that employee haven't been shown yet

	exec ShowInformation @tmpSSN

	--mark current SSN 
	INSERT INTO @tmpTable([SSN])
	VALUES (@tmpSSN)
		end
	END
	go

exec ShowEmployeeOfDepartment 'Research'
go


--4 Determine the input parameters and write a stored-procedure to show all employees which have more than n dependents.
CREATE PROCEDURE EmployeeMoreDependents(@numDependent int)
as
	BEGIN
	Declare @tmpTable TABLE(SSN char(9));
	DECLARE @tmpSSN char(9)
	
	WHILE (exists (SELECT  e.SSN
	FROM EMPLOYEE as e, DEPENDENT as d
	WHERE e.SSN = d.ESSN
	and e.SSN NOT IN (SELECT SSN FROM @tmpTable)
	Group by e.SSN
	Having count(*)> 0))

		begin
		--select @tmpSSN as top employee
		SELECT top 1 @tmpSSN = e.SSN
		FROM EMPLOYEE as e, DEPENDENT as d
		WHERE e.SSN = d.ESSN
		and e.SSN NOT IN (SELECT SSN FROM @tmpTable) -- that employee haven't been shown yet
		Group by e.SSN
		Having count(*)> 0

		exec ShowInformation @tmpSSN

		-- mark current ssn
		INSERT INTO @tmpTable([SSN])
		VALUES (@tmpSSN)
	end

	END
go


--5 Write a stored procedure which checks the following rule: �The employee should work on projects that are managed by their department�. The result will return 1 if the rule is satisfied, otherwise is 0.
CREATE PROCEDURE CheckWorkRule(@inputSSN char(9), @result int out)
AS
	BEGIN
		
		if (exists( 
		SELECT e.SSN
		FROM EMPLOYEE as e, PROJECT as p, WORKS_ON as w
		WHERE e.SSN = w.ESSN
		and w.PNO = p.PNumber
		and p.DNum != e.DNO
		and e.SSN = @inputSSN
		))
		SET @result = 0
		else
		SET @result = 1
		
	END
go

Declare @outputCheckRule1 int
exec CheckWorkRule 333445555, @outputCheckRule1 output 
print 'result ' + convert(nvarchar(50),@outputCheckRule1)

Declare @outputCheckRule2 int
exec CheckWorkRule 987987987, @outputCheckRule2 output 
print 'result ' + convert(nvarchar(50),@outputCheckRule2)
go

-- 6 Write a stored procedure which checks the following rule: �The salary of an employee should less than the salary of their department manager�. The result will return 1 if the rule is satisfied, otherwise is 0.
CREATE PROCEDURE CheckSalaryWithManager(@inputSSN char(9), @result int out)
AS 
	BEGIN
	DECLARE @tmpDNO char(9)
	DECLARE @employeeSalary int

	SELECT @employeeSalary = e.Salary, @tmpDNO = e.DNO
	FROM EMPLOYEE as e
	WHERE	e.SSN = @inputSSN

	DECLARE @managerSSN char(9)
	SELECT @managerSSN = d.MgrSSN 
	FROM DEPARTMENT as d
	WHERE d.DNumber = @tmpDNO

	DECLARE @managerSalary int
	SELECT @managerSalary = e.Salary
	FROM EMPLOYEE as e
	WHERE	e.SSN = @managerSSN

	if (@managerSalary > @employeeSalary)
		SET @result = 1
	else 
	    SET @result = 0
	END
go

DECLARE @outputCheckSalaryWithManager int
exec CheckSalaryWithManager 987987987, @outputCheckSalaryWithManager output
print 'result ' + convert(nvarchar(50),@outputCheckSalaryWithManager)
go


-- 8. Write a stored procedure to show the information of a department in the following format
CREATE PROCEDURE ShowDepartmentInformation(@inputDNumber int)
AS 
	BEGIN
	DECLARE @Name nvarchar(50)
	
	SELECT @Name = d.DName
	FROM DEPARTMENT as d
	WHERE d.DNumber = @inputDNumber

	DECLARE @NumOfEmployee int

	SELECT @NumOfEmployee = COUNT(*)
	FROM DEPARTMENT as d, EMPLOYEE as e
	WHERE d.DNumber = e.DNO
	GROUP BY d.DNumber, d.DName

	DECLARE @managerSSN char(9)
	SELECT @managerSSN = d.MgrSSN 
	FROM DEPARTMENT as d
	WHERE d.DNumber = @inputDNumber

	DECLARE @managerName nvarchar(50)
	SELECT @managerName = CONCAT(e.FName,' ',e.Minit,' ',e.LName)
	FROM EMPLOYEE as e
	WHERE e.SSN = @managerSSN


	DECLARE @NumOfProject int

	SELECT @NumOfProject = COUNT(*)
	FROM DEPARTMENT as d, PROJECT as p
	WHERE d.DNumber = @inputDNumber
	and p.DNum = d.DNumber
	GROUP BY d.DNumber,d.DName

	print 'Name of department : '+ @Name
	print 'Number of employee ' + convert(nvarchar,@NumOfEmployee)
	print 'Manager Name :'+ @managerName
	print 'Number of project ' + convert(nvarchar,@NumOfProject)

	END
go

exec ShowDepartmentInformation 4
exec ShowDepartmentInformation 1
exec ShowDepartmentInformation 5
go


--9. List all employees who belong to department @dname. Show the result in following format:
CREATE PROCEDURE ListAllEmployeeOfDepartment(@dname nvarchar(50))
AS
	BEGIN

	print 'LIST OF EMPLOYEES OF DEPARMENT: '+@dname

	DECLARE @Name nvarchar(50)
	DECLARE @DOB date
	DECLARE @Salary int

	DECLARE @c cursor
	
	SET @c = cursor for
	SELECT  CONCAT(e.FName,' ',e.Minit,' ', e.LName),  e.BDate, e.Salary
	FROM EMPLOYEE as e, DEPARTMENT as d
	WHERE d.DName = @dname
	and d.DNumber = e.DNO

	--open cursor
	OPEN @c
	FETCH NEXT FROM @c INTO @Name, @DOB, @Salary

	--Browser cursor 
	WHILE @@FETCH_STATUS =0
	BEGIN
		--using data in @Name, @DOB, @Salary
		print 'Name: '+ @Name
		print 'Date of Birth:' + convert(nvarchar, @DOB , 120) 
		print 'Salary:' + convert(nvarchar, @Salary)
	
		-- read next data
		FETCH NEXT FROM @c INTO @Name, @DOB, @Salary
	END

	CLOSE @c
	DEALLOCATE @c

	END
go


exec ListAllEmployeeOfDepartment 'Research'
go

--10. Showing information of an employee in the following format:
CREATE PROCEDURE MoreEmployeeInformation (@inputSSN int)
AS
	BEGIN
	DECLARE @Name nvarchar(50)
	DECLARE @DOB date
	DECLARE @Salary int
	DECLARE @DepartmentName nvarchar(50)
	DECLARE @NumOfDependent int
	DECLARE @NumOfProject int

	SELECT  @Name = CONCAT(e.FName,' ',e.Minit,' ', e.LName), @DOB = e.BDate, @Salary = e.Salary
	FROM EMPLOYEE as e
	WHERE @inputSSN = e.SSN

	SELECT @DepartmentName = d.DName
	FROM EMPLOYEE as e, DEPARTMENT as d
	WHERE e.SSN = @inputSSN
	and e.DNO = d.DNumber
	

	--num of dependent
	SELECT @NumOfDependent = COUNT(d.Dependent_Name)
	FROM EMPLOYEE as e
		LEFT JOIN DEPENDENT as d on e.SSN = d.ESSN
	WHERE e.SSN = @inputSSN
	Group by e.SSN
	


	--num of project
	SELECT @NumOfProject = COUNT(w.PNO)
	FROM EMPLOYEE as e
		LEFT JOIN WORKS_ON as w on e.SSN = w.ESSN
	WHERE  e.SSN = @inputSSN
	Group by e.SSN
	
	print 'Name: '+ @Name
	print 'Date of Birth:' + convert(nvarchar, @DOB , 120) 
	print 'Salary:' + convert(nvarchar, @Salary)
	print 'Department name: '  + @DepartmentName
	print 'Number of dependent ' + convert(nvarchar,@NumOfDependent)
	--more dependent here
	DECLARE @dependentName nvarchar(50)
	DECLARE @c1 cursor
	SET @c1 = cursor for
	SELECT d.Dependent_Name
	FROM DEPENDENT as d
	WHERE d.ESSN = @inputSSN

	DECLARE @countingDependent int;
	SET @countingDependent = 1
	--open cursor
	OPEN @c1
	FETCH NEXT FROM @c1 INTO @dependentName

	--Browser cursor 
	WHILE @@FETCH_STATUS =0
	BEGIN
		--using data 
		print convert(nvarchar(50),@countingDependent) + '. '+ @dependentName
		SET @countingDependent = @countingDependent +1
		-- read next data
		FETCH NEXT FROM @c1 INTO @dependentName
	END
	CLOSE @c1
	DEALLOCATE @c1



	print 'Number of project ' + convert(nvarchar,@NumOfProject)
	--more project here 
	DECLARE @projectName nvarchar(50)
	DECLARE @c2 cursor
	SET @c2 = cursor for
	SELECT p.PName 
	FROM EMPLOYEE as e, PROJECT as p, WORKS_ON as w
	WHERE e.SSN = @inputSSN
	and w.ESSN = e.SSN
	and w.PNO = p.PNumber
	

	DECLARE @countingProject int;
	SET @countingProject = 1
	--open cursor
	OPEN @c2
	FETCH NEXT FROM @c2 INTO @projectName

	--Browser cursor 
	WHILE @@FETCH_STATUS =0
	BEGIN
		--using data 
		print convert(nvarchar(50),@countingProject) + '. '+ @projectName
		SET @countingProject = @countingProject +1
		-- read next data
		FETCH NEXT FROM @c2 INTO @projectName
	END
	CLOSE @c2
	DEALLOCATE @c2

	END
go

exec MoreEmployeeInformation 123456789
exec MoreEmployeeInformation 666884444
go

-- 11 Return a list of employees whose salary is more than the average salary of �Research� department.
CREATE FUNCTION MoreSalaryThanResearch()
RETURNS @myResult table (
FName nvarchar(50),
	Minit char(1),
	LName nvarchar(50),
	SSN char(9), 
	BDate date ,
	Address nvarchar(225),
	Sex char(1),
	Salary int ,
	SupperSSN char(9),
	DNO int
)
AS 
	BEGIN
	
	DECLARE @Average int;	

	SELECT @Average = AVG(e.Salary)
	FROM EMPLOYEE as e, DEPARTMENT as d
	WHERE e.DNO = d.DNumber
	and d.DName ='Research'

	Insert @myResult
	SELECT e.*
	FROM EMPLOYEE as e
	WHERE e.Salary > @Average

	RETURN
	END
go

SELECT *
FROM MoreSalaryThanResearch()
go

--12. Return all projects which all employees don�t work in..
CREATE FUNCTION ProjectNotAllEmployee()
RETURNS @myResult TABLE (
	PName nvarchar(50),
	PNumber int PRIMARY KEY,
	PLocation nvarchar(50),
	DNum int
)
AS
	BEGIN
	
	DECLARE @Total int;
	SELECT @Total = COUNT(*)
	FROM EMPLOYEE as e

	Insert @myResult
	SELECT p.*
	FROM PROJECT as p
	WHERE not exists (
	SELECT e.SSN
	FROM EMPLOYEE as e, WORKS_ON as w
	WHERE e.SSN = w.ESSN
	and w.PNO = p.PNumber
	)
	or (
	SELECT COUNT(*)
	FROM EMPLOYEE as e, WORKS_ON as w
	WHERE e.SSN = w.ESSN
	and w.PNO = p.PNumber
	Group by w.PNO
	) < @Total

	
	RETURN 
	END
go

SELECT *
FROM ProjectNotAllEmployee()
GO

-- 13. The salary of manager must be greater than or equal to $20000.
CREATE TRIGGER ManagerSalaryEmp -- for employee table (in case manager has his salary drop) 
on EMPLOYEE
FOR UPDATE, INSERT
AS 
BEGIN

	if (exists(
	SELECT inserted.SSN
	FROM inserted , DEPARTMENT as d
	WHERE d.MgrSSN = inserted.SSN
	and inserted.Salary <20000
	))
	rollback
RAISERROR (N'Manager Salary must equal or greater than 20000', 
           10, 
           1
           );

END
go

CREATE TRIGGER ManagerSalaryDep --for department table (in case new manager have too low salary)
on DEPARTMENT
FOR UPDATE, INSERT
AS 
BEGIN

	if (exists(
	SELECT e.SSN
	FROM inserted as i , EMPLOYEE as e
	WHERE i.MgrSSN = e.SSN
	and e.Salary <20000
	))
	rollback
RAISERROR (N'Manager Salary must equal or greater than 20000', 
           10, 
           1
           );

END
go


--drop trigger ManagerSalaryEmp
--drop trigger ManagerSalaryDep

--14. Employees should be assigned only the projects managed by their department
CREATE TRIGGER AssignProject
on WORKS_ON
FOR UPDATE, INSERT
AS 
BEGIN
	
	if (exists(
	SELECT i.PNO
	FROM inserted as i, EMPLOYEE as e, PROJECT as p
	WHERE e.SSN=i.ESSN
	and i.PNO = p.PNumber
	and p.DNum != e.DNO
	))
	begin
	rollback
	RAISERROR (N'Employees should be assigned only the projects managed by their department', 
           10, 
           1
           );
	end
END
go

--15. The salary of the department�s manager should be greater than all the employees� salary of that department.
--TODO 


--16. The manager should be greater than 30 years old.
CREATE TRIGGER Manager30
on DEPARTMENT
FOR UPDATE, INSERT
AS
BEGIN

	DECLARE @mSSN int
	SELECT @mSSN = d.MgrSSN
	FROM inserted as d
	
	if (exists(
	SELECT e.SSN
	FROM EMPLOYEE as e
	WHERE e.SSN = @mSSN
	and (ABS( YEAR(GETDATE()) - YEAR(e.BDate))<=30)
	))
	begin
	rollback
	RAISERROR (N'The manager should be greater than 30 years old.', 
           10, 
           1
           );
	end
END
go

--17. An employee only works on up to 3 projects.
CREATE TRIGGER Work3Project
on WORKS_ON
FOR UPDATE, INSERT
AS 
BEGIN
	
	DECLARE @assignedSSN char(9)
	SELECT @assignedSSN = w.ESSN
	FROM inserted as w

		--num of project
		if (exists(
	SELECT e.SSN, COUNT(w.PNO)
	FROM EMPLOYEE as e
		LEFT JOIN WORKS_ON as w on e.SSN = w.ESSN
	WHERE @assignedSSN = e.SSN
	Group by e.SSN
	Having COUNT(*)>3))
	begin
	rollback
	RAISERROR (N'An employee only works on up to 3 projects. ', 
           10, 
           1
           );
	end
END
go

--18. The age of each employee should be greater than the age of his/her son or daughter.
CREATE TRIGGER DependentAge   
on DEPENDENT
FOR UPDATE, INSERT
AS 
BEGIN

	DECLARE @dependentAge int
	DECLARE @ESSN char(9)
	DECLARE @emAge int
	SELECT @dependentAge = ABS( YEAR(GETDATE()) - YEAR(inserted.BDate)), @ESSN = inserted.ESSN
	FROM inserted
	
	SELECT @emAge = ABS( YEAR(GETDATE()) - YEAR(e.BDate))
	FROM EMPLOYEE as e
	WHERE e.SSN = @ESSN
	


	if (exists(
	SELECT inserted.Dependent_Name
	FROM inserted 
	WHERE (inserted.Relationship = 'Son'
	or inserted.Relationship = 'Daughter')
	and @dependentAge > @emAge 
	))

	begin
	rollback
	RAISERROR (N'The age of each employee should be greater than the age of his/her son or daughter', 
           10, 
           1
           );
	end
END
go

--19. An employee should have maximized 1 spouse.
CREATE TRIGGER OneSpouse
on DEPENDENT 
FOR UPDATE, INSERT
AS 
BEGIN

	DECLARE @ESSN char(9)
	SELECT @ESSN = inserted.ESSN
	FROM inserted

	if (exists(
	SELECT e.SSN
	FROM DEPENDENT as d, EMPLOYEE as e
	WHERE e.SSN = @ESSN
	and d.ESSN = e.SSN
	and d.Relationship = 'Spouse'
	GROUP By e.SSN
	Having COUNT(*)>1
	))
	begin
	rollback
	RAISERROR (N'An employee should have maximized 1 spouse.', 
           10, 
           1
           );
	end

END
go

--20. If an employee and dependence have the spouse relationship, their sex must be different.
CREATE TRIGGER DifSexSpouse
on DEPENDENT 
FOR UPDATE, INSERT
AS 
BEGIN

	DECLARE @ESSN char(9)
	SELECT @ESSN = inserted.ESSN
	FROM inserted

	if (exists(
	SELECT e.SSN
	FROM DEPENDENT as d, EMPLOYEE as e
	WHERE e.SSN = @ESSN
	and d.ESSN = e.SSN
	and d.Relationship = 'Spouse'
	and d.Sex = e.Sex
	))
	begin
	rollback
	RAISERROR (N'Spouse sex must be different', 
           10, 
           1
           );
	end

END
go

--21. The maximize number employees of each department should be 3.
CREATE TRIGGER Department3Employee
on EMPLOYEE
FOR UPDATE, INSERT
AS 
BEGIN
	
	DECLARE @DNO int
	SELECT inserted.DNO
	FROM inserted
	
	if (exists(
	SELECT d.DNumber
	FROM DEPARTMENT as d, EMPLOYEE as e
	WHERE d.DNumber = e.DNO
	and d.DNumber = 4
	GROUP BY d.DNumber
	HAVING COUNT(*)>3
	))
	begin
	rollback
	RAISERROR (N'The maximize number employees of each department should be 3 ', 
           10, 
           1
           );
	end
END
go


--22. Any employee which is manager must be assigned to at least 1 project.
CREATE TRIGGER Manager1Project
ON DEPARTMENT
FOR UPDATE, INSERT
AS
BEGIN
	
	DECLARE @mSSN char(9)
	SELECT @mSSN = d.MgrSSN
	FROM inserted as d

	if (not exists(
	SELECT w.PNO
	FROM WORKS_ON as w
	WHERE w.ESSN = @mSSN
	))
	begin
	rollback
	RAISERROR (N'employee which is manager must be assigned to at least 1 project ', 
           10, 
           1
           );
	end

END
go

--23. The start date of a manager should be after his/her birth date.
CREATE TRIGGER ManagerAfterBirth
ON DEPARTMENT
FOR INSERT, UPDATE
AS
BEGIN
	DECLARE @mSSN char(9)
	SELECT @mSSN = d.MgrSSN 
	FROM inserted as d

	if (exists(
	SELECT e.SSN
	FROM EMPLOYEE as e, inserted as i
	WHERE (e.BDate>= i.MgrStartDate)
	))
	begin
	rollback
	RAISERROR (N'The start date of a manager should be after his/her birth date ', 
           10, 
           1
           );
	end
END
go

--24. The working hour number of employee per week should less than 50 hours.
CREATE TRIGGER Work50Hour
ON WORKS_ON
FOR INSERT, UPDATE
AS 
BEGIN
	DECLARE @SSN char(9)
	SELECT @SSN = i.ESSN
	FROM inserted as i

	if (exists(
	SELECT SUM(w.Hours)
	FROM EMPLOYEE as e, WORKS_ON as w
	WHERE e.SSN = @SSN
	and w.ESSN = e.SSN
	HAVING  SUM(w.Hours)>49
	))
	begin
	rollback
	RAISERROR (N'The working hour number of employee per week should less than 50 hours ', 
           10, 
           1
           );
	end

END
GO

--26. Referential integrity constraint: The department number of each employee should be exists.
CREATE TRIGGER DepartmentMustExists
ON EMPLOYEE 
FOR UPDATE, INSERT
AS
BEGIN

	if (not exists(
	SELECT d.DName
	FROM inserted as i, DEPARTMENT as d
	WHERE d.DNumber = i.DNO
	))
	begin
	rollback
	RAISERROR (N'The department number of each employee should be exists ', 
           10, 
           1
           );
	end

END
go

--27. Referential integrity constraint: The supervisor SSN of each employee should be exists.
CREATE TRIGGER SUPERMustExists
ON EMPLOYEE 
FOR UPDATE, INSERT
AS
BEGIN

	if (not exists(
	SELECT super.SSN
	FROM inserted as i, EMPLOYEE as super
	WHERE i.SupperSSN = super.SSN
	))
	begin
	rollback
	RAISERROR (N'The supervisor SSN of each employee should be exists ', 
           10, 
           1
           );
	end

END
go