


CREATE DATABASE HW01_ProjectManagement_1351040 
go


USE   HW01_ProjectManagement_1351040 
go

--Table: DEPARTMENT
--DName: nvarchar(50)
--DNumber: int
--MgrSSN: char(9)
--MgrStartDate: date
CREATE TABLE DEPARTMENT(
	DName nvarchar(50),
	DNumber int PRIMARY KEY,
	MgrSSN char(9) ,
	MgrStartDate date CHECK (MgrStartDate <= GETDATE()),
)
go

--Table: EMPLOYEE
--FName: nvarchar(50)
--Minit: char(1)
--LName: nvarchar(50)
--SSN: char(9)
--BDate: date
--Address: nvarchar(225)
--Sex: char(1)
--Salary: int
--SupperSSN: char(9)
--DNO: int
CREATE TABLE EMPLOYEE(
	FName nvarchar(50),
	Minit char(1),
	LName nvarchar(50),
	SSN char(9) PRIMARY KEY,
	BDate date CHECK (BDate <= GETDATE()),
	Address nvarchar(225),
	Sex char(1) CHECK (Sex in ('M','F')),
	Salary int CHECK (Salary>=0),
	SupperSSN char(9),
	DNO int,
)
go



--Table: DEPT_LOCATIONS
--DNumber: int
--DLocation: nvarchar(50)
CREATE TABLE DEPT_LOCATIONS(
	DNumber int ,
	DLocation nvarchar(50),
	PRIMARY KEY (Dnumber,DLocation),
)
go



--Table: PROJECT
--PName: nvarchar(50)
--PNumber: int
--PLocation: nvarchar(50)
--DNum: int
CREATE TABLE PROJECT(
	PName nvarchar(50),
	PNumber int PRIMARY KEY,
	PLocation nvarchar(50),
	DNum int,
)
go


--Table: WORKS_ON
--WORKS_ON: char(9)
--PNO: int
--Hours: float
CREATE TABLE WORKS_ON(
	ESSN char(9) ,
	PNO int,
	Hours float,
	PRIMARY KEY (ESSN,PNO),
)
go


--Table: DEPENDENT
--ESSN: char(9)
--Dependent_Name: nvarchar(50)
--Sex: char(1)
--BDate: date
--Relationship: nvarchar(50)
CREATE TABLE DEPENDENT(
	ESSN char(9),
	Dependent_Name nvarchar(50),
	Sex char(1) CHECK (Sex in ('M','F')),
	BDate date CHECK (BDate <= GETDATE()),
	Relationship nvarchar(50),
	PRIMARY KEY(ESSN,Dependent_Name),
)
go

--Table: PROJECT
--Foreign key 1: (DNum)
-- Reference table: DEPARTMENT
-- Reference Attributes: (DNumber)
ALTER TABLE PROJECT
ADD CONSTRAINT fk_PROJECT_DNum
FOREIGN KEY (DNum)
REFERENCES DEPARTMENT(DNumber)

--Table:  WORKS_ON
--Foreign key 2: (ESSN)
-- Reference table: EMPLOYEE
-- Reference Attributes: (SSN)
ALTER TABLE  WORKS_ON
ADD CONSTRAINT fk_WORKS_ON_ESSN
FOREIGN KEY (ESSN)
REFERENCES EMPLOYEE(SSN)

--Table:  WORKS_ON
--Foreign key 3: (PNO)
-- Reference table: PROJECT
-- Reference Attributes: (PNumber)
ALTER TABLE  WORKS_ON
ADD CONSTRAINT fk_WORKS_ON_PNO
FOREIGN KEY (PNO)
REFERENCES PROJECT(PNumber)

--Table:  DEPT_LOCATIONS
--Foreign key 3: (DNumber)
-- Reference table: DEPARTMENT
-- Reference Attributes: (DNumber)
ALTER TABLE DEPT_LOCATIONS
ADD CONSTRAINT fk_DEPT_LOCATIONS_DNumber
FOREIGN KEY (DNumber)
REFERENCES DEPARTMENT(DNumber)

--Table:  EMPLOYEE
--Foreign key 3: (DNO)
-- Reference table: DEPARTMENT
-- Reference Attributes: (DNumber)
ALTER TABLE EMPLOYEE
ADD CONSTRAINT fk_EMPLOYEE_DNO
FOREIGN KEY (DNO)
REFERENCES DEPARTMENT(DNumber)

--Table:  DEPENDENT
--Foreign key 3: (ESSN)
-- Reference table: EMPLOYEE
-- Reference Attributes: (SSN)
ALTER TABLE DEPENDENT
ADD CONSTRAINT fk_DEPENDENT_ESSN
FOREIGN KEY (ESSN)
REFERENCES EMPLOYEE(SSN)

--Table:  DEPARTMENT
--Foreign key 3: (MgrSSN)
-- Reference table: EMPLOYEE
-- Reference Attributes: (SSN)
ALTER TABLE DEPARTMENT
ADD CONSTRAINT fk_DEPARTMENT_MgrSSN
FOREIGN KEY (MgrSSN)
REFERENCES EMPLOYEE(SSN)
go

--Table:  EMPLOYEE
--Foreign key 3: (SupperSSN)
-- Reference table: EMPLOYEE
-- Reference Attributes: (SSN)
ALTER TABLE EMPLOYEE
ADD CONSTRAINT fk_EMPLOYEE_SupperSSN
FOREIGN KEY (SupperSSN)
REFERENCES EMPLOYEE(SSN)
go



-- Insert Employee table with NULL in SupperSSN, DNO
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('John','B','Smith','123456789','1965-01-09','731 Fondren, Houston, TX','M',30000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Franklin','C','Wong','333445555','1955-12-03','638 Voss, Houston, TX','M',40000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Alicia','J','Zelaya','999887777','1968-01-19','3321 Caslle, Spring, TX','F',25000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Jennifer','S','Wallace','987654321','1941-06-20','291 Berry, Bellaire, TX','F',40000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Ramesh','M','Narayan','666884444','1962-09-15','975 Fire Oak, Humble, TX','M',38000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Joyce','A','Enllen','453453453','1972-07-31','5631 Rice, Houston, TX','F',25000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('Ahmad','V','Jabbar','987987987','1969-03-29','980 Dallas, Houston, TX','M',25000)
INSERT INTO EMPLOYEE([FName],[Minit],[LName],[SSN],[BDate],[Address],[Sex],[Salary])
VALUES ('James','E','Borg','888665555','1937-11-10','50 Slone, Houston, TX','M',25000)
go

-- insert DEPARTMENT  
INSERT INTO DEPARTMENT([DName],[DNumber],[MgrSSN],[MgrStartDate])
VALUES ('Research','5','333445555','1938-05-22')
INSERT INTO DEPARTMENT([DName],[DNumber],[MgrSSN],[MgrStartDate])
VALUES ('Administration','4','987654321','1995-01-01')
INSERT INTO DEPARTMENT([DName],[DNumber],[MgrSSN],[MgrStartDate])
VALUES ('Headquarters','1','888665555','1931-06-19')
go

--Update Employee and dno in Employee table
UPDATE EMPLOYEE
SET SupperSSN='333445555',DNO=5
WHERE SSN='123456789';
UPDATE EMPLOYEE
SET SupperSSN='888665555',DNO=5
WHERE SSN='333445555';
UPDATE EMPLOYEE
SET SupperSSN='987654321',DNO=4
WHERE SSN='999887777';
UPDATE EMPLOYEE
SET SupperSSN='888665555',DNO=4
WHERE SSN='987654321';
UPDATE EMPLOYEE
SET SupperSSN='333445555',DNO=5
WHERE SSN='666884444';
UPDATE EMPLOYEE
SET SupperSSN='333445555',DNO=5
WHERE SSN='453453453';
UPDATE EMPLOYEE
SET SupperSSN='987654321',DNO=4
WHERE SSN='987987987';
UPDATE EMPLOYEE
SET DNO=1
WHERE SSN='888665555';

--Insert PROJECT 
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('Product Xbox',1,'Bellaire',5)
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('Product iPhone7',2,'Sugarland',5)
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('Product SS Mole',3,'Houslor',5)
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('Computerization',10,'Stafford',4)
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('Reorganization',20,'Houslor',1)
INSERT INTO PROJECT([PName],[PNumber],[PLocation],[DNum])
VALUES ('New benefits',30,'Stafford',4)
go

--Insert WORK_ON
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('123456789',1,32.5)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('123456789',2,7.5)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('666884444',3,40.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('453453453',1,20.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('453453453',2,20.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('333445555',2,10.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('333445555',3,10.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('333445555',10,10.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('333445555',20,10.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('999887777',30,30.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('999887777',10,10.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('987987987',10,35.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('987987987',30,5.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('987654321',30,20.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('987654321',20,15.0)
INSERT INTO WORKS_ON ([ESSN],[PNO],[Hours])
VALUES ('888665555',20,null)
go

--Insert DEPENDENT
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('333445555','Alice','F','1986-04-05','Daughter')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('333445555','Theodore','M','1983-10-25','Son')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('333445555','Joy','F','1958-05-03','Spouse')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('987654321','Abner','M','1942-02-23','Spouse')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('123456789','Michael','M','1988-01-04','Son')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('123456789','Alice','F','1988-01-04','Daughter')
INSERT INTO DEPENDENT ([ESSN],[Dependent_Name],[Sex],[BDate],[Relationship])
VALUES ('123456789','Elizabeth','F','1967-05-05','Spouse')
go

--Insert DEPT_LOCATIONS
INSERT INTO DEPT_LOCATIONS([DNumber],[DLocation])
VALUES (1,'Houston')
INSERT INTO DEPT_LOCATIONS([DNumber],[DLocation])
VALUES (4,'Stafford')
INSERT INTO DEPT_LOCATIONS([DNumber],[DLocation])
VALUES (5,'Bellaire')
INSERT INTO DEPT_LOCATIONS([DNumber],[DLocation])
VALUES (5,'Sugarland')
go